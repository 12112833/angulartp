import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ListPokemonComponent} from "./pokemons/list-pokemon/list-pokemon.component";
import {DetailPokemonComponent} from "./pokemons/detail-pokemon/detail-pokemon.component";
import {PageNotFoundComponent} from "./shared/page-not-found/page-not-found.component";
import {PokemonEditComponent} from "./pokemons/pokemon-edit/pokemon-edit.component";
import {AuthGuardService} from "./shared/auth-guard.service";
import {LoginComponent} from "./login/login.component";
const pokemonsRoutes: Routes = [
  {path : 'login', component: LoginComponent},

  {
    path:'pokemon',
    canActivate:[AuthGuardService],
    children:[
      {path : '', component: ListPokemonComponent, canActivate: [AuthGuardService]},
      {path:'edit/:id',component:PokemonEditComponent,canActivate:[AuthGuardService]},
      {path : ':id', component: DetailPokemonComponent, canActivate: [AuthGuardService]},
    ]
  },
  {path : '', redirectTo: 'pokemon', pathMatch: 'full'}, // quand j'ai rien il nous redirige sur la liste de pokemon
  {path: '**', component: PageNotFoundComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(pokemonsRoutes/*routes*/)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
