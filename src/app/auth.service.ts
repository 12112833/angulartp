import { Injectable } from '@angular/core';
import {Observable, of} from "rxjs";
import {delay, tap} from "rxjs/operators";


@Injectable({
  providedIn: 'root'
})
export class AuthService {
   isLoggedIn?:boolean = false ;
  redirectUrl:string = '/pokemon';

  constructor() { }
  login(name: string, password: string): Observable<boolean> {
    const isLoggedIn = (name === 'pikachu' && password === 'pikachu');
    return of(true).pipe(
      delay(1000),
      tap( val => this.isLoggedIn = isLoggedIn)
    );
  }

  logout() : void {
    this.isLoggedIn = false;
  }


}
