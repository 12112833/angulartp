import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {PokemonsService} from "../pokemons/pokemons.service";
import {AuthService} from "../auth.service";
import {Observable, of} from "rxjs";
import {delay, tap} from "rxjs/operators";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
   name!: string;
   password !:string;
    message!: string;
  constructor(private router: Router, public  authService: AuthService) { }

  ngOnInit(): void {

  }
  setMessage() {
    this.message = this.authService.isLoggedIn ?
      'Vous étes connecté.': 'Identifiant ou mot de passe incorrect .' ;
  }
  login(): void {
    this.authService.login(this.name, this.password).subscribe(_=> {
      this.setMessage();
      if (this.authService.isLoggedIn) {
        this.router.navigate([`${this.authService.redirectUrl}`])
      }
    });
  }

  logout(): void {
    this.authService.logout();
  }
}
