import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PageNotFoundComponent } from './shared/page-not-found/page-not-found.component';
import { PokemonsModule } from './pokemons/pokemons.module';
import {HttpClientJsonpModule, HttpClientModule} from "@angular/common/http";
import {HttpClientInMemoryWebApiModule} from "angular-in-memory-web-api";

import { InMemoryDataService} from "../../src/app/shared/in-memory-data.service";
import { LoginComponent } from './login/login.component' ;
import {FormsModule} from "@angular/forms";
@NgModule({
  declarations: [
    AppComponent,

    PageNotFoundComponent,
     LoginComponent,

  ],
    imports: [
        BrowserModule,
        HttpClientModule,
        HttpClientInMemoryWebApiModule.forRoot(InMemoryDataService, {dataEncapsulation: false}),
        AppRoutingModule,
        PokemonsModule,
        HttpClientJsonpModule,
        FormsModule,
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
