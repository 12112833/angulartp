import { Injectable } from '@angular/core';
import {Pokemon} from "../pokemon";
import {LIST_POKEMONS} from "../shared/list.pokemons";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable, of} from "rxjs";
import {catchError, tap} from "rxjs/operators";


@Injectable({
  providedIn: 'root'
})
export class PokemonsService {
  private pokemonsUrl = 'api/pokemons';
  constructor(private http:HttpClient) { }

  /*getListPokemons () : Pokemon[] {
    return LIST_POKEMONS;
  }*/
  getListPokemons(): Observable<Pokemon[]> {
    return this.http.get<Pokemon[]>(this.pokemonsUrl).pipe( tap( h => console.log('fetched Pokemon')),
    catchError(this.handleError('getListPokemons',[]))
  );
  }
  getPokemonTypes():string[]{
    return ['plante','Feu', 'Insect','Normal','Electrik','Poisson','Fée','Vol','Eau'];
  }

  getSinglePokemon (id : number): Observable <Pokemon> {
    /*const listPkm = this.getListPokemons();
    let pkm =new Pokemon();
    for (let i= 0 ; i< listPkm.length ; i++){
      if (id === listPkm[i].id){

        pkm =listPkm[i];
        return listPkm[i];
      }
    }
    return pkm;*/
    const url =`${this.pokemonsUrl}/${id}`;
    return this.http.get<Pokemon>(url).pipe( tap(h => console.log('Fetched Pokemon id =${id}')),
      catchError(this.handleError<Pokemon>(`Get pokemon id =${id}`))
    );
  }

  private handleError<T>(operation= 'operation' , result?: T){
    return (error :any):Observable<T> =>{
      console.log(error);
      console.log('${operation} failed:${error.message}');
      return of (result as T);
    };
  }
  updatePokemon(pokemon: Pokemon): Observable <Pokemon> {
    const httpOptions = {
      headers: new HttpHeaders({'content-type': 'application/json'})
    };
    return this.http.put(this.pokemonsUrl, pokemon, httpOptions).pipe(
      tap(h => console.log('Updated Pokemon id=${pokemon.id}')),
      catchError(this.handleError<any>('updated Pokemon'))
    );
  }
  serchPokemons(term: string ): Observable <Pokemon[]> {
    if (!term.trim()){
      return of( []);
    }
    return  this.http.get<Pokemon[]>(`${this.pokemonsUrl}/?name=${term}`).pipe(
      tap( h => console.log(`found pokemons matching "${term}"`)),
    catchError(this.handleError<Pokemon[]>('serchPokemons', []))
    );

  }

  deletePokemon(pokemon: Pokemon): Observable <Pokemon> {
    const url = `${this.pokemonsUrl}/${pokemon.id}`;
    const httpOptions = {
      headers: new HttpHeaders({'content-type': 'application/json'})
    };
    return this.http.delete<Pokemon>(url, httpOptions).pipe(
      tap(h => console.log(`Deleted Pokemon id=${pokemon.id}`)),
    catchError(this.handleError<any>('deleted Pokemon'))
  );
  }
}
