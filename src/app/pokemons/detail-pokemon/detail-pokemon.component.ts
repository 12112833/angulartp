import { Component, OnInit } from '@angular/core';
import {ActivatedRoute , Router } from "@angular/router";
import {Pokemon} from "src/app/pokemon";
import {LIST_POKEMONS} from "src/app/shared/list.pokemons";
import { PokemonsService} from "../pokemons.service";

@Component({
  selector: 'app-detail-pokemon',
  templateUrl: './detail-pokemon.component.html',
  styleUrls: ['./detail-pokemon.component.scss']
})
export class DetailPokemonComponent implements OnInit {
listOfPokemons ?:Pokemon [] ;
pokemonToDisplay?: Pokemon ;
  constructor(private route : ActivatedRoute , private  router: Router ,private pokemonsService :PokemonsService) { }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
  /* this.listOfPokemons =LIST_POKEMONS;

    for(let i = 0; i< this.listOfPokemons.length; i++){
      if (this.listOfPokemons[i].id === Number(id)){
        this.pokemonToDisplay = this.listOfPokemons[i];

      }
    }
    console.log(this.pokemonToDisplay);
*/


    const identifiant = id != null ? +id : 0;
     this.pokemonsService.getSinglePokemon(Number(id));
     this.pokemonsService.getSinglePokemon(Number(id)).subscribe(pkm => this.pokemonToDisplay = pkm);
  }

  goBack() : void{
    this.router.navigate(['pokemon']);
  }
  editerPokemon(pokemonToEdit:Pokemon): void{
    const link = ['pokemon/edit',pokemonToEdit.id];
    this.router.navigate(link);
    }

    deletePokemon(pokemonToDelete: Pokemon): void {
      this.pokemonsService.deletePokemon(pokemonToDelete).subscribe(() => {
      this.router.navigate(['pokemon']);
    });
  }

  }

