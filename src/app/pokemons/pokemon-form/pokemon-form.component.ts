import {Component, Input, OnInit} from '@angular/core';
import {Pokemon} from "src/app/pokemon";
import {PokemonsService} from "src/app/pokemons/pokemons.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-pokemon-form',
  templateUrl: './pokemon-form.component.html',
  styleUrls: ['./pokemon-form.component.scss']
})
export class PokemonFormComponent implements OnInit {
  types!: Array<string>;
  @Input() pokemon?: Pokemon ;
  constructor( private router : Router, private pokemonsService : PokemonsService) { }

  ngOnInit(): void {
    this.types =this.pokemonsService.getPokemonTypes();
  }
  hasType (type:string):boolean{
    var index =this.pokemon?.types!.indexOf(type) ;
    if (index == null ) {
      index = -1;
    }
    return (index > -1) ? true :false;
  }
  selectType($event:any,type:string):void {
    const checked = $event.target.checked;
    if(checked) {
      this.pokemon?.types?.push(type);
    }else{
      var index = this.pokemon?.types?.indexOf(type);
      if(index == null){
        index = -1;
      }
      if(index > -1){
        this.pokemon?.types?.splice(index, 1);
      }
    }
  }
  isTypesValid(type:string):boolean{
    console.log('types: ', this.pokemon?.types)
    let typeLength = this.pokemon?.types?.length;
    if(typeLength == null){
      typeLength = -1;
    }
    if(this.pokemon?.types?.length! === 1 && this.hasType(type)){
      return false;
    }
    if (this.pokemon?.types?.length! >= 3 && !this.hasType(type)){
      return false;
    }
    return true;
  }

  onSubmit(): void{
    console.log('Submit form !');
    this.pokemonsService.updatePokemon(this.pokemon!).subscribe( () => this.goBack());
   /* const link =['/pokemon', this.pokemon?.id];
    this.router.navigate(link);*/
  }
  goBack() : void{
    this.router.navigate(['pokemon']);
  }

}
