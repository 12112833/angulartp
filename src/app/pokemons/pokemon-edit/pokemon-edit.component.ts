import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { PokemonsService } from '../pokemons.service';
import {Pokemon} from "../../pokemon";

@Component({
  selector: 'app-pokemon-edit',
  templateUrl: './pokemon-edit.component.html',
  styleUrls: ['./pokemon-edit.component.scss']
})
export class PokemonEditComponent implements OnInit {

  /*singlePokemon: Pokemon = new Pokemon();*/
  singlePokemon!: Pokemon;

  constructor(private route: ActivatedRoute, private pokemonsService: PokemonsService) { }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
     this.pokemonsService.getSinglePokemon(Number(id)).subscribe( pkm => this.singlePokemon = pkm);
  }

}
