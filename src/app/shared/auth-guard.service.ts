import { Injectable } from '@angular/core';
import {ActivatedRoute, ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from "@angular/router";
import {AuthService} from "../auth.service";

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(private router: Router, private authService: AuthService) { }
  canActivate (route:ActivatedRouteSnapshot, state: RouterStateSnapshot):boolean{
    const url :string  =state.url ;
    return  this.checkLogin (url);
    /*alert('Le guard a bien été mis en place');
    return true;*/
  }

  checkLogin (url:string): boolean {
    if (this.authService.isLoggedIn) { return true; }
    this.authService.redirectUrl =url;
    this.router.navigate(['/login']);
    return false;
  }
}
