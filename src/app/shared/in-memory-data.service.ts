import { Injectable } from '@angular/core';
import { LIST_POKEMONS} from "./list.pokemons";
import { InMemoryDbService } from 'angular-in-memory-web-api';

@Injectable({
  providedIn: 'root'
})
export class InMemoryDataService implements InMemoryDbService{

  createDb() {
    const pokemons = LIST_POKEMONS;
    return { pokemons };
  }

  constructor() { }
}
