import {Directive, ElementRef, HostListener, Input} from '@angular/core';

@Directive({
  selector: '[appBorderCard]'
})
export class BorderCardDirective {
  @Input('appBorderCard') borderColor:any ;
  GREY_COLOR =  '#25fde9' ;
  GREEN_COLOR = '#008000' ;

  constructor( private element : ElementRef) {
    this.setBorder(this.GREY_COLOR);
    this.setHeight(180);
  }
  private setBorder(color: string): void{
    const border = 'solid 4px ' + color;
    this.element.nativeElement.style.border = border;
  }

  private setHeight(height: number) {
    this.element.nativeElement.style.height = height +  'px';

  }
  @HostListener('mouseenter') onMouseEnter(){
    this.setBorder( this.borderColor || this.GREY_COLOR);
  }
  @HostListener('mouseleave') onMouseLeave(){
    this.setBorder(this.GREEN_COLOR);
  }

}
